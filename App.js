import React, {Component} from 'react';
import {Provider} from 'react-redux';
import Store, {persistor} from './src/Store/Store';
import {PersistGate} from 'redux-persist/lib/integration/react';
import {NavigationContainer} from '@react-navigation/native';
import {Navigator} from './src/Navigator';

export default class App extends Component {
  render() {
    return (
      <Provider store={Store}>
        <PersistGate persistor={persistor}>
          <NavigationContainer>
            <Navigator />
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }
}
