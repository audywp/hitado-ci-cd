export const LightMode = {
  MainColor: '#FFF8DF',
  IconColor: '#BD3B04',
  SecondaryColor: '#FACAA3',
  BlackColor: '#150704',
  ButtonColor: '#D3B59C',
  BackgroundColorGradient: ['#984F0E', '#8C533D'],
};

export const DarkMode = {};
