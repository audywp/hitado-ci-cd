import React from 'react';
import {View, Text} from 'react-native';
import {Input} from 'react-native-elements';
import propTypes from 'prop-types';

function FormInput(props) {
  return (
    <View>
      <Input
        containerStyle={props.containerStyle}
        placeholderTextColor="#a0aec0"
        placeholder={props.placeholder}
        leftIcon={props.leftIcon}
        rightIcon={props.rightIcon}
        inputStyle={{color: props.Color ? props.Color : 'white'}}
        secureTextEntry={props.secureTextEntry}
        onChangeText={props.onChangeText}
        value={props.value}
        disabled={props.disabled}
      />
    </View>
  );
}

FormInput.propTypes = {
  containerStyle: propTypes.object,
  placeholder: propTypes.string,
  leftIcon: propTypes.element,
  rightIcon: propTypes.func || propTypes.element,
  secureTextEntry: propTypes.bool,
  onChangeText: propTypes.func,
};
export default FormInput;
