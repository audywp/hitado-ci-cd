import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {LightMode} from '../../Config/Colors';

export const Styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: moderateScale(100),
    width: moderateScale(100),
  },
  TextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textHeader: {
    fontSize: moderateScale(25),
    color: 'white',
    marginTop: moderateScale(10),
  },
  textChild: {
    fontSize: moderateScale(20),
    color: 'white',
  },
});
