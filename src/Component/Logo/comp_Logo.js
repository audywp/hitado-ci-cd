import React from 'react';
import {View, Text, Image} from 'react-native';
import {Styles} from './style';

export default function comp_Logo({containerStyle}) {
  return (
    <View style={containerStyle || Styles.container}>
      <View style={Styles.container}>
        <Image
          resizeMode="stretch"
          style={Styles.logo}
          source={require('../../Assets/image/Logo.png')}
        />

        <View style={Styles.TextContainer}>
          <Text style={Styles.textHeader}>Hitado</Text>
          <Text style={Styles.textChild}>Coffe Shop</Text>
        </View>
      </View>
    </View>
  );
}
