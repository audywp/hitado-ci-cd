import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {LightMode} from '../../Config/Colors';

export const Styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: moderateScale(15),
    paddingHorizontal: widthPercentageToDP(7),
  },

  Tittle: {
    textAlign: 'center',
    fontSize: moderateScale(16),
    color: LightMode.MainColor,
  },
  shadow: {
    elevation: 15,
  },
  BackgroundImages: {
    flex: 1,
  },
});
