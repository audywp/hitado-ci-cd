import React from 'react';
import {View, Text, TouchableOpacity, ImageBackground} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import Feather from 'react-native-vector-icons/Feather';
import {LightMode} from '../../Config/Colors';
import {Styles} from './style';

export default function TopHeader({height}) {
  return (
    <ImageBackground
      style={[Styles.BackgroundImages, {height}]}
      borderBottomLeftRadius={height ? moderateScale(10) : null}
      borderBottomRightRadius={height ? moderateScale(10) : null}
      source={require('../../Assets/image/Header.png')}>
      <View style={Styles.container}>
        <TouchableOpacity>
          <Feather
            name="chevron-left"
            size={moderateScale(30)}
            color={LightMode.MainColor}
            style={[Styles.Icon, Styles.shadow]}
          />
        </TouchableOpacity>
        {/* <Text style={Styles.Tittle}>{props.tittle}</Text> */}
        <TouchableOpacity>
          <Feather
            name="search"
            size={moderateScale(25)}
            color={LightMode.MainColor}
            style={[Styles.Icon, Styles.shadow]}
          />
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
}
