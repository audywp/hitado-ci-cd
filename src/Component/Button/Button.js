import React from 'react';
import {View, Text} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import propTypes from 'prop-types';
import {Styles} from './style';
import {TouchableOpacity} from 'react-native-gesture-handler';

export const NextButton = (props) => {
  return (
    <Button
      onPress={props.onPress}
      buttonStyle={[props.style ? props.style : Styles.container]}
      title={props.title}
      icon={props.icon}
      TouchableComponent={TouchableOpacity}
    />
  );
};

export const BuyButton = (props) => {
  return (
    <Button
      onPress={props.onPress}
      buttonStyle={[props.style ? props.style : Styles.container]}
      title={props.title}
      icon={props.icon}
      TouchableComponent={TouchableOpacity}
      titleStyle={props.titleStyle}
    />
  );
};

NextButton.propTypes = {
  icon: propTypes.element,
  title: propTypes.string,
  onPress: propTypes.func,
};

BuyButton.propTypes = {
  icon: propTypes.element,
  title: propTypes.string,
  onPress: propTypes.func,
};
