import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {LightMode} from '../../Config/Colors';

export const Styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
});
