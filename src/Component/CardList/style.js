import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {LightMode} from '../../Config/Colors';

export const Styles = StyleSheet.create({
  Container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: moderateScale(100),
    backgroundColor: 'white',
    paddingHorizontal: moderateScale(10),
    borderRadius: moderateScale(20),
  },
  Image: {
    height: moderateScale(80),
    width: moderateScale(80),
    borderRadius: moderateScale(80),
  },
  Desc: {
    marginLeft: moderateScale(20),
    height: '100%',
    justifyContent: 'space-between',
    paddingVertical: moderateScale(15),
  },
  RightDesc: {
    height: '100%',
    paddingVertical: moderateScale(15),
    paddingHorizontal: moderateScale(20),
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  Button: {
    width: moderateScale(70),
    height: moderateScale(20),
  },
  titleStyle: {
    fontSize: moderateScale(11),
  },
});
