import React, {useState} from 'react';
import {View, Text} from 'react-native';
import {Styles} from './style';
import FastImage from 'react-native-fast-image';
import {moderateScale} from 'react-native-size-matters';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {BuyButton} from '../../Component/Button/Button';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {LightMode} from '../../Config/Colors';

export default function CardList(props) {
  const [Fav, setFav] = useState(false);

  const favToggle = () => {
    setFav(!Fav);
  };

  return (
    <View
      style={[
        Styles.Container,
        {elevation: 2, marginBottom: moderateScale(20)},
      ]}>
      <View style={Styles.Container}>
        <FastImage
          resizeMode="center"
          source={require('../../Assets/image/items.jpg')}
          style={Styles.Image}
        />
        <View style={Styles.Desc}>
          <View>
            <Text>{props.Name}</Text>
            <Text>{props.Desc}</Text>
          </View>
          <Text>Rp {props.Price} </Text>
        </View>
      </View>

      <View style={Styles.RightDesc}>
        <TouchableOpacity onPress={favToggle}>
          <AntDesign
            name={Fav ? 'heart' : 'hearto'}
            color={Fav ? 'red' : LightMode.IconColor}
            size={moderateScale(20)}
          />
        </TouchableOpacity>
        <BuyButton
          titleStyle={Styles.titleStyle}
          title="Add Chart"
          style={Styles.Button}
        />
      </View>
    </View>
  );
}
