import React, {useState} from 'react';
import {View, ScrollView, Text, TouchableOpacityBase} from 'react-native';
import TopHeader from '../../Component/Header/Header';
import {Styles} from './style';
import {moderateScale} from 'react-native-size-matters';
import {Card, ListItem, Button, Icon} from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import {LightMode} from '../../Config/Colors';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export default function comp_ListMenu(props) {
  const [Drink, setDrink] = useState(false);
  const [Food, setFood] = useState(false);
  const [Height, setHeight] = useState(0);
  const [Widht, setWidht] = useState(0);
  const ShowOtherMenu = (card) => {
    if (card === 'drink') {
      setDrink(!Drink);
    }
    if (card === 'food') {
      setFood(!Food);
    }
  };

  const onPress = (screen) => {
    props.navigation.navigate(screen);
  };

  return (
    <ScrollView style={{flex: 1}}>
      <TopHeader height={moderateScale(180)} />
      <View style={Styles.container}>
        <Card containerStyle={Styles.Card}>
          <TouchableWithoutFeedback onPress={() => ShowOtherMenu('drink')}>
            <Card.Title>Drink</Card.Title>
            <Card.Image
              onLayout={(e) => {
                setHeight(e.nativeEvent.layout.height);
                setWidht(e.nativeEvent.layout.width);
              }}
              PlaceholderContent={
                <SkeletonPlaceholder>
                  <View style={{height: Height, width: Widht}} />
                </SkeletonPlaceholder>
              }
              resizeMode="contain"
              source={require('../../Assets/image/Cup.png')}
              style={Styles.CardImage}
            />
          </TouchableWithoutFeedback>

          {Drink ? (
            <View style={Styles.toDoList} animation="flipInY">
              <TouchableOpacity onPress={() => onPress('ListMenu')}>
                <Feather
                  name="list"
                  size={moderateScale(20)}
                  color={LightMode.IconColor}
                />
              </TouchableOpacity>
              <Text>20</Text>
              <TouchableOpacity onPress={() => onPress('CreateMenu')}>
                <Feather
                  name="plus-circle"
                  size={moderateScale(20)}
                  color={LightMode.IconColor}
                />
              </TouchableOpacity>
            </View>
          ) : null}
        </Card>
        <Card containerStyle={Styles.Card}>
          <TouchableWithoutFeedback onPress={() => ShowOtherMenu('food')}>
            <Card.Title>Food</Card.Title>

            <Card.Image
              PlaceholderContent={<SkeletonPlaceholder />}
              resizeMode="contain"
              source={require('../../Assets/image/Cake.png')}
              style={Styles.CardImage}
            />
          </TouchableWithoutFeedback>
          {Food ? (
            <View style={Styles.toDoList} animation="flipInY">
              <TouchableOpacity onPress={() => onPress('Food')}>
                <Feather
                  name="list"
                  size={moderateScale(20)}
                  color={LightMode.IconColor}
                />
              </TouchableOpacity>
              <Text>20</Text>
              <TouchableOpacity onPress={() => onPress('CreateMenu')}>
                <Feather
                  name="plus-circle"
                  size={moderateScale(20)}
                  color={LightMode.IconColor}
                />
              </TouchableOpacity>
            </View>
          ) : null}
        </Card>
      </View>
    </ScrollView>
  );
}
