import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {LightMode} from '../../../Config/Colors';

export const Styles = StyleSheet.create({
  container: {
    paddingHorizontal: widthPercentageToDP(7),
  },
  searchContainer: {
    backgroundColor: LightMode.MainColor,
  },
  inputContainer: {
    backgroundColor: 'white',
  },
});
