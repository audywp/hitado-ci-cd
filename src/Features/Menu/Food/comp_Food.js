import React from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
  FlatList,
  ScrollView,
} from 'react-native';
import {SearchBar} from 'react-native-elements';

import Feather from 'react-native-vector-icons/Feather';
import {LightMode} from '../../../Config/Colors';
import {moderateScale} from 'react-native-size-matters';
import {Styles} from './style';
import Headers from '../../../Component/Header/Header';
import CardList from '../../../Component/CardList/CardList';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

export default function comp_Food(props) {
  // should be from API
  const TempListMenu = [
    {
      id: 1,
      picture: require('../../../Assets/image/items.jpg'),
      name: 'Espresso',
      desc: 'Lorem ipsum',
      price: 22000,
    },
    {
      id: 2,
      picture: require('../../../Assets/image/items.jpg'),
      name: 'Kopi Luak',
      desc: 'Lorem ipsum',
      price: 12000,
    },
    {
      id: 3,
      picture: require('../../../Assets/image/items.jpg'),
      name: 'Kopi Item',
      desc: 'Lorem ipsum',
      price: 18000,
    },
    {
      id: 4,
      picture: require('../../../Assets/image/items.jpg'),
      name: 'Kopi ABC',
      desc: 'Lorem ipsum',
      price: 16000,
    },
    {
      id: 5,
      picture: require('../../../Assets/image/items.jpg'),
      name: 'Kopi pejuang',
      desc: 'Lorem ipsum',
      price: 32000,
    },
  ];

  const renderItem = ({item}) => {
    return <CardList Name={item.name} Desc={item.desc} Price={item.price} />;
  };

  return (
    <View
      style={{
        flex: 1,
        paddingHorizontal: widthPercentageToDP(2),
        paddingVertical: heightPercentageToDP(5),
      }}>
      <View>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={TempListMenu}
          renderItem={renderItem}
          keyExtractor={(item) => `${item.id}`}
        />
      </View>
    </View>
  );
}
