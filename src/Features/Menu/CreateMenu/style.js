import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';
import {LightMode} from '../../../Config/Colors';

export const Styles = StyleSheet.create({
  container: {
    paddingHorizontal: widthPercentageToDP(7),
    marginTop: moderateScale(30),
  },
  image: {
    height: moderateScale(120),
    width: moderateScale(120),
    borderRadius: moderateScale(120),
  },
  ImageContainer: {
    flex: 1,
    paddingVertical: moderateScale(30),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: moderateScale(30),
  },
  imageCont: {
    height: moderateScale(150),
    width: moderateScale(150),
    borderRadius: moderateScale(150),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: LightMode.MainColor,
    position: 'relative',
  },
  icon: {
    position: 'absolute',
    bottom: 0,
    right: 4,
  },
  ButtonNext: {
    paddingHorizontal: widthPercentageToDP(7),
    alignItems: 'flex-end',
  },
  buttonContainer: {
    width: moderateScale(100),
    height: moderateScale(35),
    backgroundColor: LightMode.IconColor,
  },
});
