import React from 'react';
import {View, ScrollView, Keyboard} from 'react-native';
import Input from '../../../Component/Input/Input';
import Feather from 'react-native-vector-icons/Feather';
import {moderateScale} from 'react-native-size-matters';
import LinearGradient from 'react-native-linear-gradient';
import {LightMode} from '../../../Config/Colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {Styles} from './style';
import TopHeader from '../../../Component/Header/Header';
import ImagePicker from 'react-native-image-picker';
import {Image} from 'react-native-elements';
import {
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native-gesture-handler';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {NextButton} from '../../../Component/Button/Button';

export default function CreateMenu() {
  const FormInput = [
    {
      placeholder: 'Name',
      leftIcon: (
        <MaterialCommunityIcons
          name="food"
          color={LightMode.IconColor}
          size={moderateScale(20)}
        />
      ),
    },
    {
      placeholder: 'Type',
      leftIcon: (
        <Feather
          name="key"
          color={LightMode.IconColor}
          size={moderateScale(20)}
        />
      ),
    },
    {
      placeholder: 'Price',
      leftIcon: (
        <FontAwesome
          name="money"
          color={LightMode.IconColor}
          size={moderateScale(20)}
        />
      ),
    },
  ];
  return (
    <ScrollView style={{flex: 1}}>
      <TouchableWithoutFeedback style={{flex: 1}} onPress={Keyboard.dismiss}>
        <TopHeader />
        <View style={Styles.container}>
          <View style={Styles.ImageContainer}>
            <TouchableOpacity style={Styles.imageCont}>
              <Image
                source={require('../../../Assets/image/Cup1.png')}
                style={Styles.image}
                resizeMode="contain"
                PlaceholderContent={
                  <SkeletonPlaceholder backgroundColor={LightMode.IconColor} />
                }
              />
              <Feather
                name="plus-circle"
                color={LightMode.SecondaryColor}
                size={moderateScale(40)}
                style={Styles.icon}
              />
            </TouchableOpacity>
          </View>
          {FormInput.map((value) => (
            <Input
              key={value.placeholder}
              Color={LightMode.IconColor}
              placeholder={value.placeholder}
              leftIcon={value.leftIcon}
            />
          ))}
        </View>
      </TouchableWithoutFeedback>
      <View style={Styles.ButtonNext}>
        <NextButton
          // onPress={NextButtonHandler}
          style={Styles.buttonContainer}
          title="Create"
        />
      </View>
    </ScrollView>
  );
}
