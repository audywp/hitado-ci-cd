import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

export const Styles = StyleSheet.create({
  container: {
    paddingHorizontal: widthPercentageToDP(7),
    paddingBottom: moderateScale(20),
  },
  toDoList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: moderateScale(40),
  },
  CardImage: {
    marginVertical: moderateScale(20),
  },
  Card: {
    borderRadius: moderateScale(20),
    elevation: 10,
  },
});
