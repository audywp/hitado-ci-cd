import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

export const Styles = StyleSheet.create({
  ImageBackground: {
    height: heightPercentageToDP(100),
    width: widthPercentageToDP(100),
    paddingHorizontal: widthPercentageToDP(8),
  },
  container: {
    flex: 1,
  },
  InputContainer: {
    marginTop: heightPercentageToDP(15),
  },
  logo: {
    alignItems: 'flex-start',
    marginTop: heightPercentageToDP(10),
  },
  ButtonNext: {
    position: 'absolute',
    bottom: heightPercentageToDP(10),
    right: widthPercentageToDP(8),
  },
});
