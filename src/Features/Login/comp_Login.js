import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {Styles} from './style';
//  dependencies
import Input from '../../Component/Input/Input';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import {moderateScale} from 'react-native-size-matters';
// component
import {NextButton} from '../../Component/Button/Button';
import Logo from '../../Component/Logo/comp_Logo';

export default function comp_Login (props) {
  const [PaswordVisible, setPaswordVisible] = useState (true);
  const [Password, setPassword] = useState ('');
  const [Username, setUsername] = useState ('');

  const ShowPasswordVisible = () => {
    if (Password.length) {
      return (
        <TouchableOpacity onPress={() => setPaswordVisible (!PaswordVisible)}>
          <Feather
            name={PaswordVisible ? 'eye' : 'eye-off'}
            color="white"
            size={moderateScale (20)}
          />
        </TouchableOpacity>
      );
    }
  };

  const NextButtonHandler = () => {
    if (
      Username.toLowerCase () === 'admin' &&
      Password.toLowerCase () === 'admin'
    ) {
      props.navigation.navigate ('Main');
    } else {
      Alert.alert ('error', 'Wrong Credential');
    }
  };

  const renderContent = () => {
    return (
      <ImageBackground
        resizeMode="cover"
        style={Styles.ImageBackground}
        source={require ('../../Assets/image/Coffee.png')}
      >
        <View>
          <Logo containerStyle={Styles.logo} />
        </View>
        <View style={Styles.InputContainer}>
          {/* username Input */}
          <Input
            placeholder="Username"
            leftIcon={
              <Feather name="user" color="white" size={moderateScale (20)} />
            }
            onChangeText={text => setUsername (text)}
          />
          {/* password input */}
          <Input
            placeholder="Password"
            leftIcon={
              <Feather name="lock" color="white" size={moderateScale (20)} />
            }
            secureTextEntry={PaswordVisible}
            rightIcon={ShowPasswordVisible}
            onChangeText={text => setPassword (text)}
          />
        </View>
        <View style={Styles.ButtonNext}>
          <NextButton
            onPress={NextButtonHandler}
            icon={
              <Ionicons
                name="md-enter-outline"
                color="white"
                size={moderateScale (40)}
              />
            }
          />
        </View>
      </ImageBackground>
    );
  };
  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <ScrollView style={Styles.container}>{renderContent ()}</ScrollView>
    </TouchableWithoutFeedback>
  );
}
