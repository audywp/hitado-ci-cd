import {createStore, applyMiddleware} from 'redux';
import AsyncStorage from '@react-native-community/async-storage';
import {persistStore, persistReducer} from 'redux-persist';
import logger from 'redux-logger';

import allReducers from './reducer';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const persistRootReducer = persistReducer(persistConfig, allReducers);

const Store = createStore(persistRootReducer, applyMiddleware(logger));

export const persistor = persistStore(Store);

export default Store;
