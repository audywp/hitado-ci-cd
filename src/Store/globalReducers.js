export const initialState = {
  isLogged: false,
};

const globalReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'IS_LOGGED':
      return {
        ...state,
        isLogged: true,
      };
    default:
      return state;
  }
};

export default globalReducer;
