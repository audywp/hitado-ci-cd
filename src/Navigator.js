import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
// component
import Login from './Features/Login/comp_Login';
import Menu from './Features/Menu/comp_Menu';
import CreateMenu from './Features/Menu/CreateMenu/CreateMenu';
import Food from './Features/Menu/Food/comp_Food';
import Drink from './Features/Menu/Drink/comp_Drink';
import {LightMode} from './Config/Colors';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import Headers from './Component/Header/Header';
import {moderateScale} from 'react-native-size-matters';
import {View} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Feather from 'react-native-vector-icons/Feather';
import {TouchableWithoutFeedback} from 'react-native-gesture-handler';
import {NavigationContainer} from '@react-navigation/native';

const TopBar = createMaterialTopTabNavigator();
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function TopBarNav() {
  return (
    <React.Fragment>
      <View style={{height: moderateScale(120)}}>
        <Headers height={moderateScale(100)} />
      </View>
      <TopBar.Navigator tabBarOptions={{tabStyle: {height: 50}}}>
        <TopBar.Screen name="Drink" component={Drink} />
        <TopBar.Screen name="Food" component={Food} />
      </TopBar.Navigator>
    </React.Fragment>
  );
}

export const MenuNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="Menu"
      screenOptions={{
        headerStyle: {elevation: 0},
        cardStyle: {backgroundColor: LightMode.MainColor},
      }}>
      <Stack.Screen
        name="Menu"
        component={Menu}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CreateMenu"
        component={CreateMenu}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export const BottomNavigator = () => {
  return (
    <Tab.Navigator tabBarOptions={{showLabel: false}}>
      <Tab.Screen
        options={{
          tabBarIcon: (e) => (
            <AntDesign
              name="home"
              size={moderateScale(20)}
              color={e.focused ? LightMode.IconColor : LightMode.BlackColor}
            />
          ),
        }}
        name="Menu"
        component={MenuNavigator}
      />
      <Tab.Screen
        options={{
          tabBarIcon: (e) => (
            <Feather
              name="menu"
              size={moderateScale(20)}
              color={e.focused ? LightMode.IconColor : LightMode.BlackColor}
            />
          ),
        }}
        name="ListMenu"
        component={TopBarNav}
      />
    </Tab.Navigator>
  );
};

export const Navigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Main"
        component={BottomNavigator}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};
